# Мониторинг Travellist - Laravel Demo App
(http://aixone.tk)
## Роли Ansible автоматически тестируются, версионируются и устанавливаются с помощью ansible-galaxy.
[Репа с ролями](https://gitlab.com/dpkrane-includes/ansible-roles)

## Приложение мониторится с помощью Prometheus.
Доступные exporters:
  * node_exporter
  * cadvisor
  * nginx_exporter
  * promtail

## Результат мониторинга визуализируется с помощью Grafana и Loki
